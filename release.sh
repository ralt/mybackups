#!/bin/bash

version=$1

test -z "$version" && echo "Pass new version as first argument" && exit 1

gbp dch --git-author --release --debian-tag='%(version)s' --commit --new-version="$version" --spawn-editor=never

git tag -a -m "Release $version" "$version"
