#!/bin/bash

#DEBHELPER#

source /usr/share/debconf/confmodule

test "$1" = 'configure' || exit 0

groupadd mybackups
useradd -g mybackups -r -d /home/mybackups -s /bin/bash mybackups

# It's typically bad practice to start a service in the postinst but in this
# case it's just helpful.
systemctl enable --now triptolemus@mybackups

env_file=/home/mybackups/.backups_env

db_get mybackups/s3_bucket
s3_bucket=$RET
db_get mybackups/s3_mails_folder
s3_mails_folder=$RET
db_get mybackups/s3_google_photos_folder
s3_google_photos_folder=$RET

cat > "$env_file" <<EOF
export S3_BUCKET="$s3_bucket"
export S3_MAILS_FOLDER="$s3_mails_folder"
export S3_GOOGLE_PHOTOS_FOLDER="$s3_google_photos_folder"
EOF

chown mybackups "$env_file"
chmod 600 "$env_file"

db_get mybackups/gmail_user
gmail_user=$RET
db_get mybackups/gmail_password
gmail_password=$RET

offlineimaprc=/home/mybackups/.offlineimaprc

cat > "$offlineimaprc" <<EOF
[general]
accounts = main

[Account main]
localrepository = main-local
remoterepository = gmail-remote
quick = -1

[Repository main-local]
type = Maildir
localfolders = ~/mails

[Repository gmail-remote]
type = Gmail
remoteuser = $gmail_user
remotepass = $gmail_password
nametrans = lambda foldername: re.sub(' ', '_', foldername.lower())
folderfilter = lambda foldername: foldername in {"INBOX", "[Gmail]/Sent Mail"}
sslcacertfile = /etc/ssl/certs/ca-certificates.crt
ssl = yes
ssl_version = tls1_2
EOF

chown mybackups "$offlineimaprc"
chmod 600 "$offlineimaprc"

rclone_conf=/home/mybackups/.config/rclone/rclone.conf
mkdir -p "$(dirname "$rclone_conf")"

db_get mybackups/s3_access_key_id
s3_access_key_id=$RET
db_get mybackups/s3_secret_access_key
s3_secret_access_key=$RET
db_get mybackups/s3_region
s3_region=$RET

db_get mybackups/google_photos_token
google_photos_token=$RET

cat > "$rclone_conf" <<EOF
[s3]
type = s3
provider = AWS
access_key_id = $s3_access_key_id
secret_access_key = $s3_secret_access_key
region = $s3_region
storage_class = STANDARD

[gphotos]
type = google photos
token = $google_photos_token
EOF

chown -R mybackups:mybackups /home/mybackups
chmod 600 "$rclone_conf"
