# mybackups

This repository defines a Debian package that automatically setups backups for
my Google account data to a given AWS S3 bucket.

It uses [triptolemus](https://gitlab.com/ralt/triptolemus) to manage the
synchronization jobs, and setups:

- The various scripts needed to run the jobs
- The various configuration files needed to run the jobs

The great point of that package is that I can just install it on any random VPS,
the installation process will ask me a bunch of questions (which I could
pre-define before, if I wanted a fully non-interactive installation), and
backups will go on from there.

You can then use e.g. StatusCake to setup a health check on
`http://<IP>:4242/.well-known/health` and you'll sleep in peace.

## License

MIT license.
